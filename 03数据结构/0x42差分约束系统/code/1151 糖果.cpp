#include <bits/stdc++.h>
using namespace std;
int n, k;
struct Node{
	int to, next, w; 
}e[2000050];  //不仅双向*2，还有重边 
int head[1000050];
int d[1000050];
int v[1000050];
int c[1000050];
int cnt=0; //边数 

int get_int()
{
   int x=0;
   char ch;
   for(ch=getchar();ch<'0'||ch>'9';ch=getchar());
   for(;ch>='0'&&ch<='9';ch=getchar())
     x=(x<<3)+(x<<1)+ch-'0';
   return x;
}

void add_edge(int u, int v, int w) // u-->v, 权为w
{
	e[++cnt].next = head[u];
	e[cnt].to = v;
	e[cnt].w = w;
	head[u] = cnt;
}

bool SPFA(int s)
{
	queue <int>q;
	int cur; 
	memset(v,false,sizeof(v));
	memset(d,-1,sizeof(d));
	memset(c,0,sizeof(c));
	q.push(s);
	v[s]=true;
	d[s]=0;
	while(!q.empty())
	{
		cur = q.front(); q.pop(); v[cur]=false;
		for(int ei=head[cur]; ei; ei=e[ei].next)
		{
			int to = e[ei].to; //目标结点 
			if( d[to]<d[cur]+e[ei].w) //找最大边 
			{
				d[to]=d[cur]+e[ei].w;
				if(++c[to]>=n) return false;
				if (!v[to])
				{
					v[to]=true;
					q.push(to);
				}
			}
		}
	}
	return true;
}

int main()
{
	int op, A, B;
	cin>>n>>k;
	for(int i=1;i<=k;++i)
	{
		//scanf("%d%d%d", &op, &A, &B);
		//cin>>op>>A>>B;
		op=get_int(); A=get_int(); B=get_int();
		if (op==1)	// A=B
		{
			add_edge(A, B, 0);
			add_edge(B, A, 0);
		}
		else if (op==2) // A<B => B-A>=1
		{
			if(A==B){ cout<<-1<<endl; return 0; }
			add_edge(A, B, 1);
		}
		else if (op==3) // A>=B => A-B>=0
		{
			add_edge(B, A, 0);
		}
		else if (op==4) // A>B => A-B>=1
		{
			if(A==B){ cout<<-1<<endl; return 0; }
			add_edge(B, A, 1);
		}
		else  //op==5, A<=B => B-A>=0
		{
			add_edge(A, B, 0);
		}
	}
	for (int i=n;i>=1;--i) add_edge(0, i, 1); //超级源点,逆序添加否则TLE ，卡SPFA菊花图 
	if (SPFA(0))
	{
		long long ans=0;
		for(int i=1;i<=n;++i) ans+=d[i];
		cout<<ans<<endl;
	}
	else
		cout<<-1<<endl;
	return 0;
 } 
