#include <bits/stdc++.h>
using namespace std;
struct Node{
	char data;
	int parent;
	vector <int> child;
}t[32000*63+2];
int cnt=1,root;
int main()
{
	string s;
	t[1].data='-';
	while(cin>>s)
	{
		root=1;
		for(int i=0;i<s.length();++i)
		{
			bool found=false;
			for(int j=0;j<t[root].child.size();++j)
			{
				if(s[i]==t[t[root].child[j]].data)
				{
					root=t[root].child[j];
					found=true;
					break;
				}
			}
			if(!found)
			{
				t[++cnt].data=s[i];
				t[cnt].parent=root;
				t[root].child.push_back(cnt);
				root=cnt;
			}
		}
	}
	cout<<cnt<<endl;
	return 0;
}
