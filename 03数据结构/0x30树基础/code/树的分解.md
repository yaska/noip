# 树的分解

**题目描述**
给出N个点的树和K，问能否把树划分成$N/K$个连通块，且每个连通块的点数都是K。

**输入格式**
第1 行，1 个整数T，表示数据组数。接下来T组数据，对于每组数据：
第1 行，2 个整数N,K。
接下来N−1行，每行2个整数Ai,Bi，表示边Ai-Bi。点用1,2,⋯,N 编号。

**输出格式**
对于每组数据，输出YES或NO。

**输入样例**
```
2
4 2
1 2
2 3
3 4
4 2
1 2
1 3
1 4
```
**输出样例**
```
YES
NO
```

**说明/提示**
对于60% 的数据，1≤N,K≤10^3
对于100% 的数据，1≤T≤10,1≤N,K≤10^5


## 分析

![](assets/16163121022797.jpg)
1. 后续遍历，统计以每个节点为根的子树中的结点个数。如果个数等于K，就把该子树结点置为0，表示砍掉这棵子树；
2. 如果`N%K!=0`,说明不能分解，直接输出NO；
3. 边AiBi并没有上下先后之分，因此双向建边，任意一点作为根，遍历时判重；
4. 多组数据，因此每次要把原数据的 vector 清空。

## 参考程序
```C++
#include <bits/stdc++.h>
using namespace std;
int T,	//组数据
	N,K;//N个点，分成K组
struct Node{
	vector <int> child;
}t[100005];

int dfs(int k, int last) //返回以k为根的子树的节点个数，从last点到的k点
{
    int s=0;
	for(int i=0;i<t[k].child.size();++i)
	{
		if (t[k].child[i]==last) continue;  //防止回路
		s+=dfs(t[k].child[i], k);
	}

    ++s; //加上自己
	if(s==K) s=0; //如果达到K个就砍掉
    //cout<<"k="<<k<<"; s="<<s<<endl;
	return s;
}

int main()
{
	cin>>T;
	while(T--)
	{
		cin>>N>>K;
		for(int i=1;i<=N;++i) t[i].child.clear(); //清空
		int x,y;
		for(int i=1;i<N;++i)
		{
			cin>>x>>y;
			t[x].child.push_back(y);    //双向记录，题目没有说x，y有先后之分
			t[y].child.push_back(x);    //任何一点都可以成为树根
		}
		if(N%K!=0)
		{
			cout<<"NO\n";
			continue;
		}
		if(dfs(1,1)==0) cout<<"YES\n"; else cout<<"NO\n";
	}
	return 0;
}
```
