#include <bits/stdc++.h>
using namespace std;
struct Node{
	int father;
	vector <int> child;
}t[101];
int n, m;
int root, maxid;

int main()
{
	int x, y, maxn=0;
	cin>>n>>m;
	for(int i=1;i<=m;++i)
	{
		cin>>x>>y;
		t[x].child.push_back(y);
		t[y].father = x;
		if (t[x].child.size()>maxn)
		{
			maxn=t[x].child.size();
			maxid=x;
		}
	}
	
	root=1; //从结点1开始，向上顺腾摸瓜找到根结点
	while(t[root].father!=0) root=t[root].father;
	cout<<root<<endl;
	cout<<maxid<<endl;
	sort(t[maxid].child.begin(), t[maxid].child.end()); //对maxid结点中的孩子排序
	for(int i=0; i<t[maxid].child.size(); ++i)
		cout<<t[maxid].child[i]<<" ";
	return 0;
}
