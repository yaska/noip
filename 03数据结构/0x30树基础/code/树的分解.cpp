#include <bits/stdc++.h>
using namespace std;
int T,	//组数据
	N,K;//N个点，分成K组
struct Node{
	//int parent;
	vector <int> child;
}t[100005];

int dfs(int k, int last) //返回以k为根的子树的节点个数
{
    int s=0;
	for(int i=0;i<t[k].child.size();++i)
	{
		if (t[k].child[i]==last) continue;  //防止回路
		s+=dfs(t[k].child[i], k);
	}

    ++s; //加上自己
	if(s==K) s=0; //如果达到K个就砍掉
    //cout<<"k="<<k<<"; s="<<s<<endl;
	return s;
}

int main()
{
	cin>>T;
	while(T--)
	{
		//数据读入
		cin>>N>>K;
		for(int i=1;i<=N;++i) t[i].child.clear(); //清空
		int x,y;
		for(int i=1;i<N;++i)
		{
			cin>>x>>y;
			t[x].child.push_back(y);    //双向记录，题目没有说x，y有先后之分
			t[y].child.push_back(x);    //任何一点都可以成为树根
		//	t[y].parent = x;
		}

		if(N%K!=0)
		{
			cout<<"NO\n";
			continue;
		}

		if(dfs(1,1)==0) cout<<"YES\n"; else cout<<"NO\n";
	}
	return 0;
}
