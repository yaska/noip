//P2085 最小函数值
#include <bits/stdc++.h>
using namespace std;

int n, m;
int a[10003], b[10003], c[10003], x[10003];
struct hsz
{
	int v, id;  //v是第id个函数的值
};
bool operator >(hsz a, hsz b)
{
	return a.v > b.v;
}
priority_queue<hsz, vector<hsz>, greater<hsz> >q;
int f(int a, int b, int c, int x)
{
	return a*x*x+b*x+c;
}
int main()
{
	hsz cur;
	cin>>n>>m;
	for(int i=1;i<=n;++i)
	{
		cin>>a[i]>>b[i]>>c[i];
		x[i]=1;
		cur.id=i;
		cur.v=f(a[i],b[i],c[i],x[i]);
		q.push(cur);
	}
	for(int i=1;i<=m;++i)
	{
		cur=q.top();
		q.pop();
		cout<<cur.v<<" ";
		x[cur.id]++;
		cur.v=f(a[cur.id],b[cur.id],c[cur.id],x[cur.id]);
		q.push(cur);
	}
	return 0;
}
