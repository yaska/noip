#include <bits/stdc++.h>
using namespace std;
int main()
{
    //priority_queue<int> pq;  //默认大数优先级高
    priority_queue<int,vector<int>,greater<int> > pq; //小的先出列
    int x;
    for(int i=0; i<10; i++)
    {
    	cin >> x;
    	pq.push(x);
    }
    while(!pq.empty())
    {
    	cout << pq.top() << " ";
    	pq.pop();
    }
    cout << endl << "队列内元素个数为：" << pq.size() << endl;
    return 0;
}
