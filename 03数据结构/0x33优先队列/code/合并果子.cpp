//P1090 合并果子
#include <bits/stdc++.h>
using namespace std;
priority_queue<int> q;
int n, ans=0;
int main()
{
	int w, p;
	cin>>n;
	for(int i=1;i<=n;++i)
	{
		cin>>w;
		q.push(-w);  //存入相反数，构建小根堆
	}
	for(int i=1;i<=n-1;++i) //合并n-1次
	{
		p=q.top();
		q.pop();
		p+=q.top();
		q.pop();
		ans+=(-p);
		q.push(p);
	}
	cout<<ans<<endl;
	return 0;
}
