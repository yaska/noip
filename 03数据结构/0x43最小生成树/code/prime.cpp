#include <bits/stdc++.h>
#define MAXN 100
#define INF 0x3f
using namespace std;
int n,g[MAXN][MAXN];
bool vis[MAXN];
int dist[MAXN];
int prt[MAXN];  //prt[i]=j表示g[j][i]是生成树的边 

void init()
{
	cin>>n;
	int st,ed,w;
	memset(g,INF,sizeof(g));
	while(cin>>st>>ed>>w)
		g[st][ed]=g[ed][st]=w;
	return;
}

int prime(int cur)
{
	int sum=0;
	int index;
	vis[cur]=true;
	for(int i=1;i<=n;i++) 
	{
		dist[i]=g[cur][i]; //dist[i]生成树到点i的最小距离 
		prt[i]=cur;
	}
	prt[cur]=0;
	
	for(int i=2;i<=n;i++) //加入n-1条边 
	{
		int mincost=INF;
		for(int j=1;j<=n;j++)  //找最小新边 
		{
			if(!vis[j] && dist[j]<mincost)  //!vis[j]: 点j没有被访问（j不在生成树中） 
			{
				mincost=dist[j];
				index=j;
			}
		}
		
		vis[index]=true;  //点index加入生成树 (产生一条新边)
		sum+=mincost;
		
		for(int j=1;j<=n;j++) //新点加入后，用新点（index）更新dist[] 
		{
			if (!vis[j] && g[index][j]<dist[j]) 
			{
				dist[j]=g[index][j];
				prt[j]=index;
			}
		} 
		
	}
	return sum;
}

int main()
{
	freopen("g.in","r",stdin);
	init();
	cout<<prime(1)<<endl;
	for(int i=1;i<=n;i++)
	{
		cout<<i<<" "<<prt[i]<<endl;
	}
	return 0;
}
 
