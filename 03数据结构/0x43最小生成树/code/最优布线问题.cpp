#include<bits/stdc++.h>
using namespace std;
int n,w[105][105];//w[x][y]表示连接x和y的边权
int mi[105];//mi[x]表示白点x与黑点的最小边权
bool tre[105];//tre[x]=false,x为白点；tre[x]=true,x为黑点
int k;//将要被涂黑的点 
int mst;//mst表示树的权值和
int main()
{
	freopen("ein1.txt","r",stdin);
	cin>>n;
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++)
	        cin>>w[i][j];//输入 
	memset(mi,0x7f,sizeof(mi));
	mi[1]=0; 
	for(int i=1;i<=n;i++)//每循环一次将一个点放入到树中，共需放入n个点 
	{
		k=0;
		for(int j=1;j<=n;j++)//找一个当前与黑点相连的权最小的白点 
		{
			if(tre[j]==0&&mi[j]<mi[k])
			    k=j;
		}
		tre[k]=true;
		for(int j=1;j<=n;j++)//修改与k相连的所有白点 
		{
			if(tre[j]==0&&(w[k][j]<mi[j]))
			    mi[j]=w[k][j];
		}
	}
	for(int i=1;i<=n;i++)//累加权值 
	    mst+=mi[i];
	cout<<mst;
	return 0;
}
