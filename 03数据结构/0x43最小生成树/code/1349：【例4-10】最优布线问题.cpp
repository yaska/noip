#include <bits/stdc++.h>
using namespace std;
int g[105][105];
bool vis[105];
int dist[105];
int n;

int prime()
{
	int mincost,sum=0;
	int index;
	vis[1]=true;
	for(int i=1;i<=n;i++) dist[i]=g[1][i];
	
	for(int i=2;i<=n;i++)
	{
		mincost=0x7fffffff;
		for(int j=1;j<=n;j++)
		{
			if (!vis[j] && mincost>dist[j])
			{
				mincost=dist[j];
				index=j;
			}
		}
		vis[index]=true;
		sum+=dist[index];
		for(int j=1;j<=n;j++)
		{
			if (!vis[j]&&dist[j]>g[index][j]) 
				dist[j]=g[index][j];
		}
	}
	return sum;
}
int main()
{
	cin>>n;
	memset(g,0x3f,sizeof(g));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) 
			cin>>g[i][j];
	cout<<prime()<<endl;
	return 0;
}
