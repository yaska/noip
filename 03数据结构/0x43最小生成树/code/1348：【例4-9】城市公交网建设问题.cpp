#include <bits/stdc++.h>
#define MAXN 105
#define INF 0x3f
using namespace std;
int g[MAXN][MAXN];
bool vis[MAXN];
int dist[MAXN];
int prt[MAXN];
int n,e;
void init()
{
	int u,v,w;
	memset(g,INF,sizeof(g));
	cin>>n>>e;
//	for(int i=1;i<=n;i++) g[i][i]=0;
	for(int i=1;i<=e;i++)
	{
		cin>>u>>v>>w;
		g[u][v]=g[v][u]=w;
	}
	return ;
}

void prime(int cur)
{
	int index;
	int mincost;
	vis[cur]=true;
	for(int i=1;i<=n;i++) 
	{
		dist[i]=g[cur][i];
		prt[i]=cur;
	}
	prt[cur]=0;
	
	for(int i=2;i<=n;i++)
	{
		mincost=INF;
		for(int j=1;j<=n;j++)
		{
			if(!vis[j] && mincost>dist[j])
			{
				mincost=dist[j];
				index=j;
			}
		}
		
		vis[index]=true;
		
		for(int j=1;j<=n;j++)
		{
			if(!vis[j] && dist[j]>g[index][j])
			{
				dist[j]=g[index][j];
				prt[j]=index;
			}
		}
	}
	return ;
}

int main()
{
	init();
	prime(1);
	memset(g,0,sizeof(g));
	for(int i=1;i<=n;i++)
	{
		if(prt[i]!=0) g[i][prt[i]]=g[prt[i]][i]=1;
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=i;j<=n;j++)
			if(g[i][j]) 
			{
				cout<<i<<" "<<j<<endl;
				g[i][j]=g[j][i]=0;
			}
	}

	return 0;
}
