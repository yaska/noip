#include<bits/stdc++.h>
using namespace std;
struct edge
{
	int x;
	int y; 
	int v;
} ed[9901];
int fa[101];
int n,m; 
int tot,k; 
int findfather(int x)
{
	if(fa[x]!=x) fa[x]=findfather(fa[x]);
	return fa[x];
}
void unionn(int x,int y)
{
	int f1=findfather(x);
	int f2=findfather(y);
	if(f1!=f2) fa[f1]=f2;
}
bool cmp(edge a,edge b)
{
	if(a.v <b.v) return true;
	else return false;
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	    cin>>ed[i].x>>ed[i].y>>ed[i].v;
	for(int i=1;i<=n;i++) fa[i]=i;
	sort(ed+1,ed+m+1,cmp);
	for(int i=1;i<=m;i++)
	{
		if(findfather(ed[i].x)!=findfather(ed[i].y))
		{
			unionn(ed[i].x,ed[i].y);
			tot+=ed[i].v;
			k++;
		}
		if(k==n-1)
		    break;
	}
	cout<<tot;
	return 0;
}
