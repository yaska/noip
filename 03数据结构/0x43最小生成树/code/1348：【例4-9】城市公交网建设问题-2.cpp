#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
#define inf 0x3f3f3f3f
int n,m,e[110][110];
struct Node
{
	int pre,to,n;
}dis[110],ans[110];    //dis数组不只记录最小花费，还记录左城市序号和右城市序号，ans记录答案
int k=0;               //记录ans数组中元素的个数
bool cmp(Node a,Node b)
{
	if (a.pre!=b.pre) return a.pre<b.pre;
	else return a.to<b.to;
}
void prim()
{
	int book[110]={0};
	for (int i=1;i<=n;i++)           //记录花费的同时，记录从哪来到哪去
	{
		dis[i].n=e[1][i];
		dis[i].pre=1;
		dis[i].to=i;
	}
	book[1]=1;
	int minn,u;
	for (int i=0;i<n-1;i++)
	{
		minn=inf;
		for (int j=1;j<=n;j++)
		{
			if (!book[j]&&dis[j].n<minn)
			{
				minn=dis[j].n;
				u=j;
			}
		}
		if (dis[u].pre<dis[u].to)           //保证左城市的序号小于右城市的序号
		    ans[k].pre=dis[u].pre,ans[k].to=dis[u].to;
		else ans[k].pre=dis[u].to,ans[k].to=dis[u].pre;
		k++;
		book[u]=1;
		for (int j=1;j<=n;j++)
		      if (!book[j]&&dis[j].n>e[u][j])//重新更新起点城市到其它城市的最短距离
		      {
				dis[j].n=e[u][j];
				dis[j].pre=u;     //记录这个最短距离的从哪个城市到哪个城市
				dis[j].to=j;
		      }
	}
}
int main ()
{
	cin>>n>>m;
	int u,v,w;
	memset(e,inf,sizeof(e));
	for (int i=1;i<=n;i++)
	    e[i][i]=0;
	for (int i=0;i<m;i++)
	{
		cin>>u>>v>>w;
		e[u][v]=w;
		e[v][u]=w;
	}
	prim();
	sort(ans,ans+k,cmp);
	for (int i=0;i<k;i++)
		cout<<ans[i].pre<<" "<<ans[i].to<<endl;
	return 0;
}
