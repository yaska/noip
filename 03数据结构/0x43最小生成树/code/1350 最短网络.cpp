#include<bits/stdc++.h>
using namespace std;
struct edge
{
	int x;
	int y;//x,y表示边的两个端点 
	int v;//表示边的权值 
} ed[9901];
int fa[101];//fa[x]表示点x所在集合的头领 
int n,x,m;//m表示边数 
int tot,k;//to表示总权值，k表示树的边数 
int findfather(int x)//找头领 
{
	if(fa[x]!=x) fa[x]=findfather(fa[x]);
	return fa[x];
}
void unionn(int x,int y)//合并集合 
{
	int f1=findfather(x);
	int f2=findfather(y);//使合并效率更高 
	if(f1!=f2) fa[f1]=f2;
}
bool cmp(edge a,edge b)
{
	if(a.v <b.v) return true;
	else return false;
}
int main()
{
	freopen("ein2.txt","r",stdin);
	cin>>n;
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++)
	    {
	    	cin>>x;
	    	if(x!=0)//有一条边 
	    	{
	    		m++;
	    		ed[m].x=i;
	    		ed[m].y=j;
	    		ed[m].v=x;
			}
		}
	for(int i=1;i<=n;i++) fa[i]=i;
	sort(ed+1,ed+m+1,cmp);
	for(int i=1;i<=m;i++)
	{
		if(findfather(ed[i].x)!=findfather(ed[i].y))
		{
			unionn(ed[i].x,ed[i].y);
			tot+=ed[i].v;
			k++;
		}
		if(k==n-1)//数已生成； 
		    break;
	}
	cout<<tot;
	return 0;
}
