#include <bits/stdc++.h>
using namespace std;
#define lowbit(x) ((x)&(-x))
int N, k;
int c[500005];

inline void update(int i, int x)
{
	for(int pos = i; pos <= N; pos += lowbit(pos))
		c[pos] += x;
}

inline int query(int n)
{
	int sum = 0;
	for(int pos = n; pos; pos -= lowbit(pos))
		sum += c[pos];
	return sum;
}

int main()
{
	char opt[3];
	int m, p;
	scanf("%d%d", &N, &k);
	while(k--)
	{
		scanf("%s", &opt);
		//cout<<"k="<<k<<" opt="<<opt<<endl;
		if(opt[0] == 'A')
		{
			scanf("%d", &m);
			printf("%d\n", query(m));
		}
		else if(opt[0] == 'B')
		{
			scanf("%d%d", &m, &p);
			update(m, p);
		}
		else
		{
			scanf("%d%d", &m, &p);
			update(m, -p);
		}
	}
	return 0;
}


 
