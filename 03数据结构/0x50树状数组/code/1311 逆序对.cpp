// Created on iPadpro2.

#include <bits/stdc++.h>
using namespace std;
#define MAXN 100005
#define lowbit(x) ((x) & (-x))
long long c[MAXN];
long long sum=0;

void show(int n)
{
    for(int i=1; i<=n; ++i) printf("%4d", c[i]);
    puts("\n");
}
inline void update(int i, int x)
{
    for(int pos = i; pos <= MAXN ; pos += lowbit(pos))
        c[pos] += x;
}

inline int query(int n)
{
    long long ans = 0;
    for(int pos = n; pos; pos -= lowbit(pos))
        ans += c[pos];
    return ans;
}

int main() 
{
    int n, x;
    scanf("%d", &n);
    for(int i=1; i<=n; ++i)
    {
        scanf("%d", &x);
        update(x, 1);
        sum += (i - query(x));
        //show(n); printf("sum=%d\n",sum);
    }
    printf("%lld\n", sum);
    return 0;
}