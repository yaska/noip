# 敌兵布阵

**Problem Description**
C国的死对头A国这段时间正在进行军事演习，所以C国间谍头子Derek和他手下Tidy又开始忙乎了。A国在海岸线沿直线布置了N个工兵营地,Derek和Tidy的任务就是要监视这些工兵营地的活动情况。由于采取了某种先进的监测手段，所以每个工兵营地的人数C国都掌握的一清二楚,每个工兵营地的人数都有可能发生变动，可能增加或减少若干人手,但这些都逃不过C国的监视。
中央情报局要研究敌人究竟演习什么战术,所以Tidy要随时向Derek汇报某一段连续的工兵营地一共有多少人,例如Derek问:“Tidy,马上汇报第3个营地到第10个营地共有多少人!”Tidy就要马上开始计算这一段的总人数并汇报。但敌兵营地的人数经常变动，而Derek每次询问的段都不一样，所以Tidy不得不每次都一个一个营地的去数，很快就精疲力尽了，Derek对Tidy的计算速度越来越不满:"你个死肥仔，算得这么慢，我炒你鱿鱼!”Tidy想：“你自己来算算看，这可真是一项累人的工作!我恨不得你炒我鱿鱼呢!”无奈之下，Tidy只好打电话向计算机专家Windbreaker求救,Windbreaker说：“死肥仔，叫你平时做多点acm题和看多点算法书，现在尝到苦果了吧!”Tidy说："我知错了。。。"但Windbreaker已经挂掉电话了。Tidy很苦恼，这么算他真的会崩溃的，聪明的读者，你能写个程序帮他完成这项工作吗？不过如果你的程序效率不够高的话，Tidy还是会受到Derek的责骂的.

**Input**
第一行一个整数T，表示有T组数据。
每组数据第一行一个正整数N（N<=50000）,表示敌人有N个工兵营地，接下来有N个正整数,第i个正整数ai代表第i个工兵营地里开始时有ai个人（1<=ai<=50）。
接下来每行有一条命令，命令有4种形式：
(1) Add i j, i和j为正整数,表示第i个营地增加j个人（j不超过30）
(2) Sub i j , i和j为正整数,表示第i个营地减少j个人（j不超过30）;
(3) Query i j ,i和j为正整数,i<=j，表示询问第i到第j个营地的总人数;
(4) End 表示结束，这条命令在每组数据最后出现;
每组数据最多有40000条命令

**Output**
对第i组数据,首先输出“Case i:”和回车,
对于每个Query询问，输出一个整数并回车,表示询问的段中的总人数,这个数保持在int以内。

**Sample Input**

```
1
10
1 2 3 4 5 6 7 8 9 10
Query 1 3
Add 3 6
Query 2 7
Sub 10 2
Add 6 3
Query 3 10
End
```

**Sample Output**

```
Case 1:
6
33
59
```

**source**

[Problem - 1166 (hdu.edu.cn)](http://acm.hdu.edu.cn/showproblem.php?pid=1166)


<div style=“page-break-after:always;”></div>

# 树状数组

**概念**

树状数组（Binary Indexed Tree），用数组来模拟树形结构，最早由Peter M.Fenwick 于1994年以 《A New Data Structure for Cumulative Frequency Tables》为题发表在 《SOFTWARE PRACTICE  AND EXPERIENCE》。
其目的是解决数据压缩里的累积频率（Cumulative Frequency）的计算问题。

**操作**

最简单的树状数组支持2种操作：

* 单点修改：更改数组种的一个元素值；
* 区间查询：查询一个区间内所有元素的和。

两种操作的复杂度都是**O(logn)**。

## 传统方法

普通数组
用一个数组`a[]`存储每个兵营的人数，`a[i]`存放第i个兵营的人数。则：
单点修改的时间复杂度是$O(1)$；
区间求和需要循环整个区间的长度，时间复杂度是$O(n)$。

前缀和
用b数组存放前缀和，即`b[i]`存放`a[1]+a[2]+...+a[i]`。则：
单点修改需要修改受影响的所有b结点，时间复杂度是$O(n)$；
求区间 [x,y] 的和，只需要`b[y]-b[x-1]`，时间复杂度是`O(1)`。

## 树状数组原理

树状数组借用前缀和的思想，建立一个c数组记录部分数据的和，提高区间求和的效率。利用二进制分解原理，将区间分拆分成了不重叠的部分，在进行单点修改时，只修改受影响的区间，提高修改效率。

比如，求num=321的前缀和。
将 321=300+20+1 按数位分段，可以表示为：
$$
f[321] = (0,300] + (300,320] + (321,321]
$$

求前缀和的时间复杂度为：$log_{10}{n}$，即n的位数；
单点修改只需要修改包含到单点的数据，如修改a[5]，则需要修改区间：(0,300]，时间复杂度也是$log_{10}{n}$。

由于10进制的每一位有10个数要分别分段，程序复杂，因此用二进制分解，每一位只有0和1两种数据，从而简化数据结构。如num=13=$(1101)_2$，则可以表示为：
$$
f[13]=f[(1101)_2]=(0000,1000]+(1000,1100]+(1100,1101]
$$

![](media/16179669373168.jpg)

每一段区间就是“去掉二进制数最右边一个1”。于是构成树状数组C[i]，如图：

<img src="media/image-20210409213903063.png" alt="image-20210409213903063" style="zoom:50%;" />

```
c[1] = a[1]
c[2] = a[1] + a[2]
c[3] = a[3]
c[4] = a[1] + a[2] + a[3] + a[4]
c[5] = a[5]
c[6] = a[5] + a[6]
c[7] = a[7]
c[8] = a[1] + a[2] + a[3] + a[4] + a[5] + a[6] + a[7] + a[8]
```





  ## 树状数组实现

为了描述方便，定义：

`lowbit(x)`为x二进制最右边的第一个1，以及连带它之后的0。如：$lowbit(14) = lowbit(1100_2) = 100_2 $

 `c[i]`为树状数组，c[i] 维护区间 （i-lowbit(i), i] 之间的数据和。



### lowbit(x) 函数的实现

```c++
#define lowbit(x) ((x) & (-x))
```



### 单点修改

修改某结点的数值，则需要将受到影响的所有c[]结点更新。

 修改结点a[i]，则受影响的最小树状数组元素是c[i]，以后依次加上c[]元素下标的lowbit值。如修改a[3]，则需要修改：
$$
c[3]=c[0011_2] \stackrel{lowbit(3)}{\longrightarrow} 
c[4]=c[0100_2] \stackrel{lowbit(4)}{\longrightarrow} 
c[8]=c[1000_2]
$$

  ```C++
int c[MAXN];
inline void update(int i, int x){ //在i结点加上x
    for(int pos = i; pos <= MAXN; pos += lowbit(pos))
        c[pos] += x;
}
  ```

​                                                                                                                                                                                                                                                                                                                                                                                         

### 求前n项和

按二进制位分段求和，如求前13项的和
$$
\begin{aligned}
f[13] = f[(1101)_2]&=(0000,1000]+(1000,1100]+(1100,1101] \\
 &= c[1000_2] + c[1100_2] + c[1101_2] \\
 &= c[8] + c[12] + c[13] \\
 &= (a[1]+a[2]+...+a[8]) + (a[9]+a[10]+a[11]+a[12])+(a[13]) 
 \end{aligned}
$$

通过不断去掉二进制的最后一个1来找到每个区间：
$$
c[13] = c[1101_2] \stackrel{-lowbit(1101_2)}{\longrightarrow} 
c[1100_2] \stackrel{-lowbit(1100_2)}{\longrightarrow}
c[1000_2] \stackrel{-lowbit(1000_2)}{\longrightarrow} 
c[0](结束)
$$


```C++
inline int query(int n){
    int ans = 0;
    for(int pos = n; pos; pos -= lowbit(pos)) //每循环一次，去掉最后一个1
        ans += c[pos];
    return ans;
}
```



### 区间查询

直接使用前缀和思想

```C++
inline int query(int a, int b){
    return query(b) - query(a-1);
}
```



# 参考程序

```C++
#include <bits/stdc++.h>
using namespace std;
#define lowbit(x) ((x) & (-x))
#define MAXN 50004
int c[MAXN];

void show(int n)
{
	for(int i=1;i<=n;++i)
		cout<<setw(4)<<c[i];
	cout<<endl;
}

inline void update(int i, int x)
{
	for(int pos = i; pos <= MAXN; pos += lowbit(pos))
		c[pos] += x;
}

inline int query(int n)
{
	int ans = 0;
	for(int pos = n; pos; pos -= lowbit(pos))
		ans += c[pos];
	return ans;
}

inline int query(int a, int b)
{
	return query(b) - query(a - 1);
}

int main()
{
	int T, N, x, a, b;
	char opr[10];
	
	scanf("%d", &T);
	for(int I=1; I<=T; ++I)
	{
		printf("Case %d:\n", I);
		memset(c, 0, sizeof(c));
		scanf("%d", &N);
		for(int i=1; i<=N; ++i )
		{
			scanf("%d", &x);
			update(i, x);
		} 
		while(scanf("%s", opr), opr[0]!='E')
		{
			if(opr[0]=='A')
			{
				scanf("%d%d", &a, &b);
				update(a, b);
			}
			else if(opr[0]=='S')
			{
				scanf("%d%d", &a, &b);
				update(a, -b);
			}
			else
			{
				scanf("%d%d", &a, &b);
				printf("%d\n", query(a, b));
			}
		}
	}

	return 0;
}
```



# 练习

数列操作 http://ybt.ssoier.cn:8088/problem_show.php?pid=1535



