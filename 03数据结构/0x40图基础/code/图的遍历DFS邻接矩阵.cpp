/* dfs 遍历图，有向图 
//5个节点 6条路
5 6 
1 2
1 3
2 3
2 5
4 1
5 3
*/ 
#include <bits/stdc++.h>
using namespace std;
#define MAXN 10001
int g[MAXN][MAXN];
bool v[MAXN];
int n,m;
void show()
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++) cout<<g[i][j]<<" ";
		cout<<'\n';
	}
}
void dfs(int i)
{
	v[i]=true;
	cout<<i<<" ";
	for(int j=1;j<=n;j++)
		if(!v[j] && g[i][j]) dfs(j);
}
int main()
{
	int x,y;
	cin>>n>>m;
	for(int i=1;i<=m;i++) 
	{
		cin>>x>>y;
		g[x][y]=1;
	}
	show();
	for(int i=1;i<=n;i++) //非连通图无法从一个节点遍历完，所以对每个未访问的节点进行遍历 
		if(!v[i]) dfs(i);
	return 0;
}
