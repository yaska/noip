#include <bits/stdc++.h>
using namespace std;
#define MAXN 10001
int g[MAXN][MAXN];
int dis[MAXN]; //起点s到每个节点的距离(无权图，相邻节点距离1) 
bool v[MAXN];
int n,m;
void show()
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++) cout<<g[i][j]<<" ";
		cout<<'\n';
	}
}
void bfs(int s)
{
	int q[MAXN],tail,head;
	tail=head=0;
	q[head]=s; dis[s]=0; v[s]=true;
	cout<<s<<":"<<dis[s]<<'\n';
	while(head<=tail) //队列不为空
	{
		int curr=q[head++];  //队首出队
		for(int i=1;i<=n;i++)
		{
			if(!v[i] && g[curr][i])
			{
				v[i]=true;
				dis[i]=dis[curr]+1;
				q[++tail]=i;
				cout<<i<<":"<<dis[i]<<'\n';
			} 
		} 
	} 
}

int main()
{
	int x,y;
	cin>>n>>m;
	for(int i=1;i<=m;i++) 
	{
		cin>>x>>y;
		g[x][y]=1;
	}
	for(int i=1;i<=n;i++)
		if(!v[i]) bfs(i);
	return 0;
}
