#include <bits/stdc++.h>
using namespace std;
struct Edge{
	int to;
	int v; //1表示有路，0表示没有路
};
vector <Edge> e[2001*2]; //无向图，双向建边
int degree[101]; //结点的度
vector <int> path; //路径
int n, m;

void dfs(int x)
{
	for (auto &u : e[x]) //遍历结点x的所有边 u, 因为后面要修改u的值，所以添加引用&
	{
		if (u.v == 1) // x -> u.to 有路
		{
			u.v = 0;
			for (auto &k : e[u.to]) // 遍历 u.to 的所有边 k
				if (k.to == x) { k.v = 0; break; } //删除回边
			dfs(u.to);
		}
	}
	path.push_back(x);
}

int main()
{
	cin >> n >> m;
	for (int i=0; i<m; i++)
	{
		int x, y; //结点x与结点y之间有一条路
		cin >> x >> y;
		e[x].push_back({y,1});
		e[y].push_back({x,1});
		++degree[x];
		++degree[y];
	}
	int starter = 1; // 起点
	int cnt = 0;        // 奇数度点的个数
	for (int i=1; i<=n; i++)
	{
		if (degree[i] % 2 == 1)
		{
			++cnt;
			starter = i;
		}
	}
	if (cnt==0 || cnt==2)
	{
		dfs(starter);
		for (int i=path.size()-1; i>=0; i--)
			cout << path[i] << " ";
	}
	else
		cout << "-1" << endl;
	return 0;
}
