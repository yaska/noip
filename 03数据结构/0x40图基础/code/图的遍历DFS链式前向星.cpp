/* dfs 遍历图，有向图 
//5个节点 6条路
5 6 
1 2
1 3
1 4
2 3
2 5
5 1 
*/ 
#include <bits/stdc++.h>
using namespace std;
#define MAXN 10001
#define MAXE 10000001 
int head[MAXN];
struct Edge{
	int next;
	int to;
}edge[MAXE];
bool v[MAXN];
int n,m,e_num=0;
void add_edge(int u,int v)
{
	edge[++e_num].next=head[u];
	edge[e_num].to=v;
	head[u]=e_num;
}
void dfs(int i)
{
	v[i]=true;
	cout<<i<<" ";
//	int e=head[i];
//	while(e!=0)
//	{
//		if(!v[edge[e].to]) dfs(edge[e].to);
//		e=edge[e].next;
//	}
	for(int e=head[i];e;e=edge[e].next)
		if(!v[edge[e].to]) dfs(edge[e].to);
}
int main()
{
	int x,y;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		cin>>x>>y;
		add_edge(x,y);
	}
	for(int i=1;i<=n;i++)
		if(!v[i]) dfs(i);
	return 1;
}
