#include <bits/stdc++.h>
using namespace std;
const int N = 1010; //图的最大点数量
struct Edge {       //记录边的终点，边权的结构体
    int to;         //终点
    int value;      //边权
};
int n, m; //表示图中有n个点，m条边
vector<Edge> p[N];  //使用vector的邻接表


int main() {
    cin >> n >> m;
    //m条边
    for (int i = 1; i <= m; i++) {
        int u, v, l;                //点u到点v有一条权值为l的边
        cin >> u >> v >> l;
        p[u].push_back({v, l});
    }

    //输出
    for (int i = 1; i <= n; i++) {
        printf("出发点：%d ", i);
//        for (int j = 0; j < p[i].size(); j++)
//            printf(" 目标点：%d,权值：%d;", p[i][j].to, p[i][j].value);
		for (Edge e: p[i])
			printf(" 目标点：%d，权值：%d;", e.to, e.value);
        puts("");
    }

    return 0;
}

/**
 * 测试数据
 4 6
 2 1 1
 1 3 2
 4 1 4
 2 4 6
 4 2 3
 3 4 5
 */
