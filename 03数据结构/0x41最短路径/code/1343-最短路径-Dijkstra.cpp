#include <bits/stdc++.h>
using namespace std;
struct Point{
	int x,y;
}node[101];
double mp[101][101];
double d[101]; 
bool v[101];
int n,m;
int s,t;

int main()
{
	//读入数据，邻接矩阵存储 
	int x,y;
	cin>>n;
	for(int i=1;i<=n;i++) cin>>node[i].x>>node[i].y;
	cin>>m;
	memset(mp,127,sizeof(mp)); // 1.38242e+306，可以相加 
	for(int i=1;i<=m;i++)
	{
		cin>>x>>y;
		mp[y][x]=mp[x][y]=sqrt(pow(node[x].x-node[y].x,2)+pow(node[x].y-node[y].y,2)); 
	}
	
	//Dijkstra O(n^2)				
	cin>>s>>t;
	memset(d,127,sizeof(d));
	for(int i=1;i<=n;i++) d[i]=mp[s][i];
	d[s]=0;
	v[s]=true;
	for(int i=1;i<=n-1;i++) //每次固定一个点 
	{
		//找到最短路 
		double ml=1e308;  
		int k=0;
		for(int j=1;j<=n;j++)
			if(!v[j] && d[j]<ml)
			{
				k=j;
				ml=d[j];
			}
		if(k==0) break; // 出现非连通图，没有出现更短路，提前终止 
		v[k]=true;
		for(int j=1;j<=n;j++)
			if(!v[j] && d[k]+mp[k][j]<d[j]) d[j]=d[k]+mp[k][j];
	}
		
	cout<<fixed<<setprecision(2)<<d[t]<<endl;
	return 0;
}
 
