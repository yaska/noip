#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
struct Edge  //前向星存储 
{
	int next,to;
	int w;
}e[1001];
int node[1001];
bool v[1001]; //判断节点是否在队列中 
int cnt[1001]; //记录节点入队次数 
int d[1001];
int n,m,edge_num=0;
void show(int d[])
{
	for(int i=1;i<=n;i++)
		if (d[i]>INF/2) cout<<"INF"<<" "; else cout<<d[i]<<" ";
	cout<<endl;
}
void add_edge(int u, int v, int w)
{
	e[++edge_num].next=node[u];
	e[edge_num].to=v;
	e[edge_num].w=w;
	node[u]=edge_num;
} 
bool spfa(int s)
{
	queue <int>Q; 
	int cur;//队首节点
	int ei;//边 
	int tmp; //cur节点的目标节点 
	memset(v,false,sizeof(v));
	memset(d,0x3f,sizeof(d));
	memset(cnt,0,sizeof(cnt));
	Q.push(s);
	v[s]=true;
	d[s]=0;
	while(!Q.empty())
	{
		cur=Q.front(); Q.pop(); v[cur]=false;  //队首出队
		ei=node[cur];
		while(ei)  // 遍历cur开始的边边 
		{
			tmp=e[ei].to; 
			if(d[tmp]>d[cur]+e[ei].w) //如果松弛成功 
			{
				d[tmp]=d[cur]+e[ei].w;
				if(!v[tmp])  //如果目标节点不在队列中 
				{
					Q.push(tmp); v[tmp]=true;
					if(++cnt[tmp]>n) return false;  //判断负回路 
				}
			}
			ei=e[ei].next;
		}
	}
	return true;
}
int main()
{
	int u,v,w;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		cin>>u>>v>>w;
		add_edge(u,v,w);
	}
	if(spfa(1)) show(d);

	return 0;
}

