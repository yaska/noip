//Dijkstra 邻接矩阵
#include <bits/stdc++.h>
using namespace std;
#define N 1001  
int n,m; //n个顶点，m条边 
int g[N][N], v[N], d[N]; //g图，v是否访问过，d距离
int p[N]; // p[i]=j 表示 j-->k 是一条最优路径，用于递归输出 
int r[N]; //路径

void showg()//显示邻接矩阵 
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			if(g[i][j]<0x3f3f3f3f) cout<<setw(5)<<g[i][j];
			else cout<<setw(5)<<"INF";
		cout<<endl;
	}
}
void show(int a[])
{
	for(int i=1;i<=n;i++)
		if(a[i]<0x3f3f3f3f) cout<<setw(5)<<a[i];
		else cout<<setw(5)<<"INF";
	cout<<endl;
}

void dijkstra(int s) //求s到任意点距离 
{
	memset(v,false,sizeof(v));
	memset(d,0x3f,sizeof(d)) ;
	d[s]=0;
	for(int i=1;i<=n;i++) //循环n次，每次确定一个结点 
	{
		int mi=0x3fffffff, id;  //mi当前最短d[i]，id最短d[i]的节点编号i 
		for(int j=1;j<=n;j++)
			if (!v[j] && d[j]<=mi) mi=d[j], id=j;  //第一次循环一定是id=s 
		v[id]=true; //标记已经访问
		for(int k=1;k<=n;k++)			//从新标记的节点id开始松弛操作，实际上这里更新的是所有点，但是与x未相邻的w[x][y]值是无穷大，不可能被松弛 
		{	
			//d[k]=min(d[k],d[id]+g[id][k]);   //要么不变d[k]，要么通过节点id再到k 
			if(d[id]+g[id][k]<d[k])
			{
				d[k]=d[id]+g[id][k];
				p[k]=id;	
			} 
		} 
		//show(v);show(d);    //检查v，d是否正确 
	}	
}

int main()
{
	int x,y,w;
	
	//读入图 
	cin>>n>>m;
	memset(g,0x3f,sizeof(g));
	for(int i=1;i<=m;i++)
	{
		cin>>x>>y>>w;
		g[x][y]=w;
	}
	//showg();  //检查图是否输入正确 
	//找结点1到所有点的最短路 
	dijkstra(1);
	
	//输出
	for(int i=1;i<=n;i++)
	{ 
		cout<<i;
		if(d[i]==0x3f3f3f3f) 
			cout<<":INF\n";
		else
		{
			int j=i;
			while(j!=1) 
			{
				cout<<" "<<p[j];
				j=p[j];
			} 
			cout<<": "<<d[i]<<endl;
		}	
	}	
	return 0; 
} 
 


/* 
6 8
1 3 10
1 5 30
1 6 100
2 3 5
3 4 50
4 6 10
5 6 60
5 4 20

v1 为起点：
v1-->v1 = 0
v1-->v2 = INF
v1-->v3 = 10
v1-->v5--v4 = 50
v1-->v5 = 30
v1-->v5--v4--v6 = 60
*/ 
