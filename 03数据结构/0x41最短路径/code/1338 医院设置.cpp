#include <bits/stdc++.h>
using namespace std;
int m[101][101];
int p[101];
int n;
int ans=0x7fffffff;
void show()
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++) cout<<m[i][j]<<" ";
		cout<<'\n';
	}
	return;
}
int main()
{
	int lc,rc;
	int tot=0;
	cin>>n;
	memset(m,0x3f,sizeof(m)); 
	for(int i=1;i<=n;i++)   //邻接表存储数据,无向图 
	{
		m[i][i]=0;
		cin>>p[i]>>lc>>rc;
		if(lc) m[i][lc]=m[lc][i]=1;
		if(rc) m[i][rc]=m[rc][i]=1;
	}

	//floyd求最短路 
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(m[i][j]>m[i][k]+m[k][j])
				 	m[i][j]=m[i][k]+m[k][j];
				
	//穷举最短路
	for(int i=1;i<=n;i++) //以i为医院节点
	{
		tot=0;
		for(int j=1;j<=n;j++)
			tot+=p[j]*m[i][j];
		if (ans>tot) ans=tot;
	} 
	cout<<ans<<endl;
	return 0;
 } 
