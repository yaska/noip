#include <bits/stdc++.h>
using namespace std;
int a[801][801]; //floyd求任意两点间最短距离
int c[801]; //各牧场拥有奶牛数
int N, P, C;
int ans=0x7fffffff;

void floyd(int n) //对n个结点求最短路径
{
    for(int k=1;k<=n;++k)
        for(int i=1;i<=n;++i)
            for(int j=1;j<=n;++j)
                if(a[i][k]+a[k][j]<a[i][j])
                    a[i][j]=a[i][k]+a[k][j];
}


int main()
{
    cin>>N>>P>>C;
    for(int i=1;i<=N;++i) //读入牧场奶牛数
    {
        int x;
        cin>>x;
        ++c[x];
    }
    memset(a,0x3f,sizeof(a));
    for(int i=1;i<=C;++i) //读入地图
    {
        int x,y,z;
        cin>>x>>y>>z;
        a[x][y]=a[y][x]=z;
    }
    for(int i=1;i<=P;++i) a[i][i]=0;
    floyd(P);

    int sum=0;
    for(int i=1;i<=P;++i)
    {
        sum=0;
        for(int j=1;j<=P;++j)
        {
            sum+=a[i][j]*c[j];
        }
        ans = ans>sum?sum:ans;
    }
    cout<<ans<<endl;
    return 0;
}