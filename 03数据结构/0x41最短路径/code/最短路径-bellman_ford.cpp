/* Bellman-Ford 模板 */ 
#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
struct Edge
{
	int u,v;  //u-->v
	int w;    //权 
}e[1001];     //采用边集存储 
int d[1001];
int pre[1001];  //前序 
int n,m;
void show(int d[])
{
	for(int i=1;i<=n;i++)
		if (d[i]>INF/2) cout<<"INF"<<" "; else cout<<d[i]<<" ";
	cout<<endl;
}
bool bellman_ford(int s)  //O(NE)
{   
	//初始化 
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	//n-1次松弛 
	for(int i=1;i<=n-1;i++)  
	{
		bool flag=false;
		for(int j=1;j<=m;j++)
			if (d[e[j].v]>d[e[j].u]+e[j].w) 
			{
				d[e[j].v]=d[e[j].u]+e[j].w; //松弛
				//pre[e[j].v]=e[j].u;       //记录路径 
				flag=true; 
			}
		show(d);  //查看d数组，检查松弛效果 
		if(!flag) break;  //一轮下来没有松弛 
	}
	//判断回路
	for(int j=1;j<=m;j++)
		if (d[e[j].v]>d[e[j].u]+e[j].w) 
			return false;
	return true; 
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>e[i].u>>e[i].v>>e[i].w;

	if(bellman_ford(1))
		show(d);
		//show(pre);
	return 0;
} 

/*
5 8
1 2 -1
1 3 4
2 3 3
2 4 2
2 5 2
4 3 5
4 2 1
5 4 -3
1

out:
0 -1 2 -2 1

5 8
1 3 3
1 4 6
2 1 2
3 5 -2
4 2 -1
4 3 5
5 1 7
5 4 4
*/



 
