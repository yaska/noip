#include <bits/stdc++.h>
using namespace std;
const int N=2002, M=10001;
int n, m;
int g[N][N];
int d[N], p[N];
bool v[N];

void print(int s)
{
	if(p[s]!=0) print(p[s]);
	cout<<s<<" ";
}

void dijkstra(int s)//s点出发的最短路径
{
	memset(d, 0x3f, sizeof(d));
	d[s]=0;
	
	for(int i=1;i<=n;++i)
	{
		int k=0; //最短节点编号
		for(int j=1; j<=n; ++j)
			if(!v[j] && d[j]<d[k]) k=j;

		v[k]=true; //标记访问（染色）
		
		for(int j=1; j<=n; ++j)
		{
			if(!v[j] && d[k]+g[k][j]<d[j]) //松弛操作
			{
				d[j]=d[k]+g[k][j];
				p[j]=k;
			}
		}
	}
}

int main()
{
	cin>>n>>m;
	int x,y,w;
	memset(g, 0x3f, sizeof(g));
	for(int i=1;i<=m;++i)
	{
		cin>>x>>y>>w;
		g[x][y]=g[y][x]=w;
	}
	dijkstra(1);

	for(int i=1;i<=n;++i)
	{
		cout<<i<<"("<<d[i]<<") ";
		print(i);
		cout<<endl;
	}

	return 0;
}

/*
5 7
1 2 2
2 5 2
2 3 1
1 3 4
3 5 6
3 4 1
1 4 7

ans:
1(0) 1
2(2) 1 2
3(3) 1 2 3
4(4) 1 2 3 4
5(4) 1 2 5
*/
	
