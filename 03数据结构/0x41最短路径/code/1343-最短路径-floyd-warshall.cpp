#include <bits/stdc++.h>
using namespace std;
struct Point{
	int x,y;
}node[101];
double mp[101][101];
int n,m;
int s,t;

int main()
{
	//读入数据，邻接矩阵存储 
	int x,y;
	cin>>n;
	for(int i=1;i<=n;i++) cin>>node[i].x>>node[i].y;
	cin>>m;
	memset(mp,127,sizeof(mp)); // 1.38242e+306，可以相加 
	for(int i=1;i<=m;i++)
	{
		cin>>x>>y;
		mp[y][x]=mp[x][y]=sqrt(pow(node[x].x-node[y].x,2)+pow(node[x].y-node[y].y,2)); 
	}
	cin>>s>>t; //读入起点和终点 
	
	//Floyd-Warshall O(n^3) ,三重循环 
	for(int k=1;k<=n;k++)  //中间结点 
		for(int i=1;i<=n;i++)  //起点 
			for(int j=1;j<=n;j++)  //终点 
				if(mp[i][j]>mp[i][k]+mp[k][j]) mp[i][j]=mp[i][k]+mp[k][j];
				
	cin>>s>>t;
	cout<<fixed<<setprecision(2)<<mp[s][t]<<endl;
	return 0;
}

/*
5 
0 0
2 0
2 2
0 2
3 1
5 
1 2
1 3
1 4
2 5
3 5
1 5

out：
3.41 
