#include <bits/stdc++.h>
using namespace std;
struct Node{
    int to, next, w;
}g[15002];
int cnt=0;
int head[2502];
int d[2502];
int T, C, Ts, Te;
void add_edge(int from, int to, int w)
{
    g[++cnt].next = head[from];
    g[cnt].to = to;
    g[cnt].w =w;
    head[from] = cnt;
}

void dijsktra(int s)
{
    priority_queue<pair<int,int>, vector<pair<int, int> >, greater<pair<int,int> > > q; //pair<距离,编号>
    bool v[2502];

    memset(v, false, sizeof(v));
    memset(d, 0x3f, sizeof(d));
    d[s]=0;
    q.push(make_pair(0,s));
    while(!q.empty())
    {
        pair<int, int> cur = q.top(); q.pop();
        if (v[cur.second]) continue;
        v[cur.second]=true;
      //  cout<<cur.second<<":"<<cur.first<<endl;
        for(int e=head[cur.second]; e; e=g[e].next)
        {
           // cout<<"  ending:"<<g[e].to<<v[g[e].to]<<" "<<d[g[e].to]<<" "<<d[cur.second]<<" " <<g[e].w<<endl;
            if(!v[g[e].to] && d[g[e].to] > d[cur.second] + g[e].w)
            {
                d[g[e].to] = d[cur.second] + g[e].w;
                q.push(make_pair(d[g[e].to], g[e].to));
             //   cout<<"  push:"<<g[e].to<<endl;
            }
        }
    }
}

int main()
{
    scanf("%d %d %d %d", &T, &C, &Ts, &Te);
    for(int i=1;i<=C;++i)
    {
        int x, y, z;
        scanf("%d %d %d", &x, &y, &z);
        add_edge(x, y, z);
        add_edge(y, x, z);
    }

    dijsktra(Ts);

    printf("%d\n", d[Te]);
    return 0;
}