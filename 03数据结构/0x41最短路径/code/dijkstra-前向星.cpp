#include <bits/stdc++.h>
using namespace std;
const int N=2002, M=10001;
int n, m, cnt=0;  //cnt边数
int head[2002];
struct Edge{
	int v, w, next;
}edge[M*2]; //双向建边，数量x2
bool v[N];
int d[N],p[N];

void print(int s)
{
	if(p[s]!=0) print(p[s]);
	cout<<s<<" ";
}

void add_edge(int u, int v, int w)
{
	edge[++cnt].next = head[u];
	edge[cnt].v = v;
	edge[cnt].w = w;
	head[u] = cnt;
}

void dijkstra(int s)
{
	memset(d, 0x3f, sizeof(d));
	d[1]=0;
	for(int i=1;i<n;++i) //
	{
		int k=0;  //最小值位置
		for(int j=1;j<=n;++j)
		{
			if(!v[j] && d[j]<d[k]) k=j;
		}
		v[k]=true;

		for(int e=head[k]; e; e=edge[e].next)
		{
			if(!v[edge[e].v] && d[edge[e].v] > d[k]+edge[e].w)
			{
				d[edge[e].v] = d[k]+edge[e].w;
				p[edge[e].v] = k;
			}
		}
	}
}

int main()
{
	cin>>n>>m;
	int u,v,w;
	for(int i=1;i<=m;++i)
	{
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}

	dijkstra(1);
	
	for(int i=1;i<=n;++i)
	{
		cout<<i<<"("<<d[i]<<") ";
		print(i);
		cout<<endl;
	}
	return 0;
}
