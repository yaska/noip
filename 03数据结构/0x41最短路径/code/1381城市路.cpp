//1381 城市路 Dijkstra
#include <bits/stdc++.h>
using namespace std;
int n, m, cnt=0;  //cnt边数 
int head[2002];
struct Edge{
	int v, w, next;
}edge[20002]; 
bool v[2002];
int d[2002];

void add_edge(int u, int v, int w)
{
	edge[++cnt].next = head[u];
	edge[cnt].v = v;
	edge[cnt].w = w;
	head[u] = cnt;
}

void dijkstra(int s)
{
	memset(d, 0x3f, sizeof(d));
	d[s]=0;
	for(int i=1;i<n;++i) //
	{
		int k=0;  //最小值位置 
		for(int j=1;j<=n;++j)
		{
			if(!v[j] && d[j]<d[k]) k=j;
		}
		v[k]=true; 

		for(int e=head[k]; e; e=edge[e].next)
		{
			if(!v[edge[e].v] && d[edge[e].v] > d[k]+edge[e].w)
				d[edge[e].v] = d[k]+edge[e].w;
		}
	}
}

int main()
{
	cin>>n>>m;
	int u,v,w;
	for(int i=1;i<=m;++i)
	{
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	 
	dijkstra(1);
	cout<<d[n]<<endl;
	return 0;
}

/*
5 5
1 2 20
2 3 30
3 4 20
4 5 20
1 5 100

ans:
90

result:
测试点1	答案正确	464KB	5MS
测试点2	答案正确	468KB	6MS
测试点3	答案正确	468KB	5MS
测试点4	答案正确	488KB	5MS
测试点5	答案正确	480KB	7MS
测试点6	答案正确	516KB	10MS
测试点7	答案正确	716KB	40MS
测试点8	答案正确	732KB	42MS
测试点9	答案正确	712KB	42MS
测试点10	答案正确	724KB	42MS
测试点11	答案正确	708KB	40MS
*/
