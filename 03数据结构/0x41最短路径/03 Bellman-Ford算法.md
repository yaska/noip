# Bellman-Ford算法

图的单源最短路径算法。  

图的任意一条最短路径既不能包含负权回路，也不会包含正权回路，因此它最多包含n-1条边。

从源点s可达的所有顶点如果存在最短路径，则这些最短路径构成一个以s为根的最短路径树。  

Bellman-Ford算法的迭代松弛操作，实际上就是按每个点实际的最短路径[虽然我们还不知道，但它一定存在]的层次，逐层生成这棵最短路径树的过程。

每一次遍历，都可以从前一次遍历的基础上，找到此次遍历的部分点的单源最短路径。

因此，如果在经历了n-1次松弛操作后，还能进行松弛，说明图中含有负回路。

## 条件

1. 单源最短路径
2. 可以有负边，但不能有负回路

## 算法描述

1. 初始化。用d数组表示源点s到其他各点的最短距离，d[s]=0, d[i]=inf;

2. 计算最短路径，执行 n- 1 次遍历； 

   对于图中的每条边：如果起点 u 的距离 d 加上边的权值 w 小于终点 v 的距离 d，则更新终点 v 的距离值 d；

3. 检测途中是否有负回路。遍历图中的所有边，计算 u 至 v 的距离，如果对于 v 存在更小的距离，则说明存在负回路。

整体时间复杂度为 O(VE)，V是点的个数，E是边的个数。



```C++
bool BellmanFord(list vertices, list edges, vertex source)
   // 该实现读入边和节点的列表，并向两个数组（distance和predecessor）中写入最短路径信息
 
   // 步骤1：初始化图
   for each vertex v in vertices:
       if v is source then distance[v] := 0
       else distance[v] := infinity
       predecessor[v] := null
 
   // 步骤2：重复对每一条边进行松弛操作
   for i from 1 to size(vertices)-1:
       for each edge (u, v) with weight w in edges:
           if distance[u] + w < distance[v]:
               distance[v] := distance[u] + w
               predecessor[v] := u
 
   // 步骤3：检查负权环
   for each edge (u, v) with weight w in edges:
       if distance[u] + w < distance[v]:
           error "图包含了负权环"
```

## 代码分析

```C++
/* Bellman-Ford 模板 */ 
#include <bits/stdc++.h>
#define INF 0x3f3f3f3f
using namespace std;
struct Edge
{
	int u,v;  //u-->v
	int w;    //权 
}e[1001];     //采用边集存储 
int d[1001];
int pre[1001];  //前序 
int n,m,s;
void show(int d[])
{
	for(int i=1;i<=n;i++)
		if (d[i]>INF/2) cout<<"INF"<<" "; else cout<<d[i]<<" ";
	cout<<endl;
}
bool bellman_ford(int s)  //O(NE)
{   
	cout<<"s="<<s<<endl;
	//初始化 
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	//n-1次松弛 
	for(int i=1;i<=n-1;i++)  
	{
		for(int j=1;j<=m;j++) //j：m-->1 效果不同
			if (d[e[j].v]>d[e[j].u]+e[j].w) 
			{
				d[e[j].v]=d[e[j].u]+e[j].w; //松弛
				//pre[e[j].v]=e[j].u;       //记录路径 
			}
		show(d);  //查看d数组，检查松弛效果 
	}
	//判断回路
	for(int j=1;j<=m;j++)
		if (d[e[j].v]>d[e[j].u]+e[j].w) 
			return false;
	return true; 
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>e[i].u>>e[i].v>>e[i].w;
	//cin>>s;
	for(s=1;s<=n;s++)
	if(bellman_ford(s))
	{
		show(d);  //show(pre);
	}
	return 0;
} 
```

![g1](assets/g1-1556100378466.png)

相同的数据，搜索边的顺序不同，效率不同。

从右向左遍历边

![g2](assets/g2.png)





从左向右遍历边

![g2](assets/g2-1621307766684.png)







分析1：Bellman-Ford算法经常会在未达到n-1（最大值）轮松弛前就已经计算出最短路。因此可以加个变量判断d数组中的数据有没有发生改变，如果一轮下来，数据没有改变，就直接退出。

```c++
	for(int i=1;i<=n-1;i++)  
	{
		bool flag=false;  //添加标记
		for(int j=1;j<=m;j++)
			if (d[e[j].v]>d[e[j].u]+e[j].w) 
			{
				d[e[j].v]=d[e[j].u]+e[j].w; //松弛
				//pre[e[j].v]=e[j].u;       //记录路径 
				flag=true; 
			}
		show(d);  //查看d数组，检查松弛效果 
		if(!flag) break;  //一轮下来没有松弛 
	}
```

分析2：只有被上一次松弛操作影响过的节点，才会参与到下一次的松弛。因此，每次松弛操作只需要遍历被松弛过的节点的边。在实际操作中通过队列来实现，称为 **SPFA** 算法。

分析3：例子数据用一论就能得出结论，因为样例图是DAG（有向无环图），我们可以构造一个**拓扑升序序列**，由拓扑排序的性质，**无环图的任意路径中，顶点都是沿着「拓扑升序序列」排列的**，因此我们只需要按照这个序列执行更新操作，这样可以在线性时间内解决问题。

分析4：与Dijkstra不同的是，Dijkstra的基本操作“拓展”是在深度上寻路，而“松弛”操作则是在广度上寻路，这就确定了贝尔曼-福特算法可以对负边进行操作而不会影响结果。

分析5：负回路。直观看来，如果图中出现负回路，我们只需要在这个回路里面转圈圈，就能实现最小路径负无穷。根据理论，n个节点的图中，最短路径不超过n-1条边，因此当进行了n-1轮松弛操作后，如何还可以松弛，就可以判断出有负回路了。





# SPFA算法

SPFA 算法是 Bellman-Ford算法 的队列优化算法的别称，通常用于求含负权边的单源最短路径，以及判负权环。SPFA 最坏情况下复杂度和朴素 Bellman-Ford 相同，为 O(VE)。因此在没有负边权时，应该尽量使用dijkstra算法。

设立一个先进先出的队列用来保存待优化的结点，优化时每次取出队首结点u，并且用u点当前的最短路径估计值对离开u点所指向的结点v进行松弛操作，如果v点的最短路径估计值有所调整，且v点不在当前的队列中，就将v点放入队尾。这样不断从队列中取出结点来进行松弛操作，直至队列空为止。

## 代码分析

``` C++
#include <bits/stdc++.h>
using namespace std;
#define MAXN 100005
#define MAXM 500000
int M, N;
struct Edge{
    int to, next;
    int w;
}g[MAXM*2+1];  //无向图，双倍建边
int head[MAXN];
int cnt=0; //边数cnt
int d[MAXN];

void add_edge(int from, int to, int w)
{
    g[++cnt].next = head[from];
    g[cnt].to = to;
    g[cnt].w = w;
    head[from] = cnt;
}
bool spfa(int s)
{
    queue<int> q;
    bool v[MAXN];  //v[i]=true; i结点在队列中
    int c[MAXN];  //c[i]=j;    i结点入队j次
    memset(d, 0x3f, sizeof(d));
    memset(v, false, sizeof(v));
    memset(c, 0, sizeof(c));

    d[s]=0;
    q.push(s);
    v[s]=true;
    ++c[s];
    while(!q.empty())
    {
        int cur=q.front(); q.pop();
        v[cur]=false;
        for(int e=head[cur]; e; e=g[e].next) //遍历cur结点所有边
        {
            // cur: 起点; e:边的编号; g[e].to 终点; g[e].w 边权
            if( d[g[e].to]>d[cur]+g[e].w)
            {
                d[g[e].to]=d[cur]+g[e].w;  //松弛
                if(!v[g[e].to]) 
                {
                    q.push(g[e].to);
                    v[g[e].to]=true;
                    if(++c[g[e].to]>N) return false;
                }
            }
        }//end for
    }// end while(!q.empty())
    return true;
}

int main()
{
    scanf("%d %d\n", &N, &M);
    for(int i=1;i<=M;++i)
    {
        int x, y, w;
        scanf("%d %d %d\n", &x, &y, &w);
        add_edge(x, y, w);
        add_edge(y, x, w);
    }
    if(spfa(1))
        printf("%d\n",d[N]);
    else
        puts("impossible");
    return 0;
}
```





# 小结

![g3](assets/g3.png)

