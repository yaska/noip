#include <bits/stdc++.h>
using namespace std;
struct node{
    int data;
    int fa, lc, rc;
}t[105];
int n, x, c=0;
bool found=false;

void inorder(int r, int x)
{
    if(found) return;
    if(t[r].lc!=0) inorder(t[r].lc, x);
    ++c;
    if(t[r].data==x)
    {
        cout<<c<<endl;
        found=true;
    }
    if(t[r].rc!=0) inorder(t[r].rc, x);
}
        

int main()
{
    cin>>n>>x;
    for(int i=1;i<=n;++i)
    {
        cin>>t[i].data>>t[i].lc>>t[i].rc;
        t[t[i].lc].fa=t[t[i].rc].fa=i;
    }
    int root=1;
    while(t[root].fa!=0) root=t[root].fa;
    inorder(root, x);
    return 0;
}
