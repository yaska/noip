# 并查集

## 例题 强盗团伙

哥谭市有很多犯罪份子，罗宾发现这些犯罪份子分属于不同的犯罪团伙。现在罗宾想要查出到底有多少个不同的犯罪团伙。

**输入**

第一行，两个整数n和m（1<=m<n<=10000），表示有n个犯罪份子，m组关系。

接下来m行，每行2个整数a，b，表示犯罪份子a和犯罪份子b是同伙（属于同一个犯罪团伙）。

**输出**

一个整数，表示犯罪团伙数。

注意：罪犯同伙的同伙也属于同一个犯罪团伙。

**样例输入**

```
10 9
1 2
3 4 
5 2
4 6
2 6
8 7
9 7
1 6
2 4
```



**样例输出**

```
3
```

### 问题分析

判断某个罪犯是否属于某个团伙，即判断某个元素是否属于某个集合内，可以用穷举的方式来模拟，复杂度为$O(n^2)$。也可以利用”并查集“建立类似树形结构快速判断。

<div style='page-break-after:always;'></div> 

<div style='page-break-after:always;'></div> 

## 知识点：并查集

### 概念

**并查集**是一种用来管理元素分组情况的数据结构。并查集包含多个集合，每个集合可能包含一个或多个元素，并选出一个集合中的元素作为这个集合的代表。并查集关心（能解决的）问题是：

1. **查询**给定元素属于哪一个集合，通常返回这个集合的代表元素；
2. **判断**元素a和元素b是否属于同一个集合；
3. **合并**元素a和元素b所在集合为一个集合。

这些操作的时间复杂度是常数级的。

**并查集常见用途：**

·    求连通子图

·    求最小生成树的Kruskal算法

·    求最近公共祖先（Least Common Ancestors, LCA）

·    维护无向图的连通性。支持判断两个点是否在同一连通块内，判断增加一条边是否会产生环。

### 基本操作

1. makeset(s)：建立并查集，其中包含s个单元素集合。
2. unionset(x,y)：合并元素x和元素y的集合。
3. find(x)：找到元素x所在集合的代表。如果两个元素的代表相同，说明两个元素在同一个集合。

用一个一维数组来存放每个犯罪团伙的信息，f[i]=j，表示罪犯i属于j的团队（可以理解为j是这个犯罪团伙的“头领”）。由样例数据可以得出，我们需要将10个元素根据其相关性分成多个集合，每个集合有1个或多个元素，在具体程序设计时，选集合中的哪个作为代表（头领）并不重要，关键是其它元素从属于个头领元素，构成树形结构。

**1. 初始化 makeset**

建立集合。在没有得知相关关系时，每个元素独立构成一个集合。即10个罪犯就是10个团伙，团伙的“头领”就是自己。

```C++
void makeset(int n)
{
    for(int i=1;i<=n;i++) f[i]=i;
    return ;
}
```

![](assets/16165554073771.jpg)





**2.查找 find**

查找元素x所在的集合，返回该集合的代表（头领）。

如果自己就是头领（集合代表），则直接返回自己，否则去找自己直属老大的老大。

![](assets/16165555340637.jpg)

```c++
int find(int x)
{
    if (f[x]==x]) return x; // 自己就是头领
    return find(f[x]);      // 递归查找头领的头领
}
```

路径压缩

我们在查找元素8所在集合时，进行了三次递归，其中依次访问了4，3，1号元素，说明8，4，3，1是同一个集合，在递归返回时，顺便将数组f[3],f[4],f[8]的值修改为该集合的代表元素值，则可以将下次查询这些元素的时间复杂度降为常数。

![](assets/16165554921477.jpg)


```C++
int find(int x)
{
    if (f[x]==x) return x;
    f[x] = find(f[x]);
    return f[x];
}
```



**3. 合并 unionset**

将 x 和 y 元素所在的集合合并为同一个集合。

如果 x 和 y 在同一个团伙，则不用合并；否则 y 的头领带全体小弟并入 x 集合。

```C++
void unionset(int x, int y) 
{
	x=find(x);
	y=find(y);
	if(x!=y) f[y]=x;
	return;
}
```



## 例题参考程序：强盗团伙

```C++
#include <iostream>
using namespace std;
int f[10001];

void makeset(int n)  //初始化并查集 
{
	for(int i=1;i<=n;i++) f[i]=i;
	return;
}

int find(int x)
{
    if (f[x]==x) return x;
    f[x] = find(f[x]);
    return f[x];
}

void unionset(int x, int y) 
{
	x=find(x);
	y=find(y);
	if(x!=y) f[y]=x;
	return;
}

int main()
{
	int n,m,x,y,ans=0;;
	cin>>n>>m;
	makeset(n); 
	for(int i=1;i<=m;i++)
	{
		cin>>x>>y;
		unionset(x,y);
	}
	for(int i=1;i<=n;i++)
		if (i==find(i)) ans++; // 自己的就是代表，说明是一个团伙 
	cout<<ans<<endl;
	return 0;	
}
```



## 练习

亲戚 http://ybt.ssoier.cn:8088/problem_show.php?pid=1389

家谱 http://ybt.ssoier.cn:8088/problem_show.php?pid=1388

格子游戏 http://ybt.ssoier.cn:8088/index.php

最优布线 http://ybt.ssoier.cn:8088/problem_show.php?pid=1349

Milk Visits S https://www.luogu.com.cn/problem/P5836

