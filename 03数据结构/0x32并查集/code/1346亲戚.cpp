//1346 亲戚
#include <bits/stdc++.h>
using namespace std;
int f[20004];
int fa[20004];
int s[20004];
int n,m,q;

int ff(int x)
{
	if(f[x]==x) return x;
	f[x]=ff(f[x]);
	return f[x];
}

int ff2(int x) //消去递归，还是超时，已经过的9个点更慢
{
	int top=0;
	while(f[x]!=x)
	{
		s[++top]=x;
		x=f[x];
	}
	while(!top)
	{
		f[s[top--]]=x;
	}
	return x;
}

void uf(int x, int y)
{
	x=ff(x);
	y=ff(y);
	if(x!=y) f[y]=x;
}


int main()
{
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i) f[i]=i;
	
	int x,y;
	for(int i=1;i<=m;++i)
	{
		scanf("%d%d",&x,&y);
		uf(x,y);
	}
	
	//提前将f路径压缩一遍
	for(int i=1;i<=n;++i)
		f[i]=ff(i);
	
	cin>>q;
	for(int i=1;i<=q;++i)
	{
		scanf("%d%d",&x,&y);
		if(f[x]!=f[y]) puts("No"); else puts("Yes");
	}
	return 0;
}


