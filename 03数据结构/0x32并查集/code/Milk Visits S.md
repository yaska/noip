
## Milk Visits s

Farmer John 计划建造 N 个农场，用 N−1 条道路连接，构成一棵树（也就是说，所有农场之间都互相可以到达，并且没有环）。每个农场有一头奶牛，品种为更赛牛或荷斯坦牛之一。

Farmer John 的 M个朋友经常前来拜访他。在朋友 i 拜访之时，Farmer John 会与他的朋友沿着从农场 $A_i$ 到农场$B_i$之间的唯一路径行走（可能有 $A_i = B_i$）。除此之外，他们还可以品尝他们经过的路径上任意一头奶牛的牛奶。由于 Farmer John 的朋友们大多数也是农场主，他们对牛奶有着极强的偏好。他的有些朋友只喝更赛牛的牛奶，其余的只喝荷斯坦牛的牛奶。任何 Farmer John 的朋友只有在他们访问时能喝到他们偏好的牛奶才会高兴。

请求出每个朋友在拜访过后是否会高兴。。

### 输入

输入的第一行包含两个整数 N 和 M。

第二行包含一个长为 N 的字符串。如果第 i 个农场中的奶牛是更赛牛，则字符串中第 i 个字符为 G，如果第 i 个农场中的奶牛是荷斯坦牛则为 H。

以下 N−1 行，每行包含两个不同的整数 X 和 Y（1≤X,Y≤N），表示农场 X 与 Y 之间有一条道路。

以下 M 行，每行包含整数 $A_i，B_i$ ，以及一个字符 $C_i$ 。$A_i$  和 $B_i$  表示朋友 i 拜访时行走的路径的端点，$C_i$是 G 或 H 之一，表示第 i 个朋友喜欢更赛牛的牛奶或是荷斯坦牛的牛奶。

### 输出

输出一个长为 *M* 的二进制字符串。如果第 i 个朋友会感到高兴，则字符串的第 i个字符为 `1`，否则为 `0`。

### 样例输入

```
5 5
HHGHG
1 2
2 3
2 4
1 5
1 4 H
1 4 G
1 3 G
1 3 H
5 5 H
```



### 样例输出

```
10110
```



### 说明 说明/提示

在这里，从农场 1 到农场 4 的路径包括农场 1、2 和 4。所有这些农场里都是荷斯坦牛，所以第一个朋友会感到满意，而第二个朋友不会。

关于部分分：

测试点 1 样例。

测试点 2∼5 满足 $N\le 10^3，M\le 2\cdot 10^3$。

对于 100% 的数据，$1 \leq N \leq 10^5，1 \leq M \leq 10^5$。

https://www.luogu.com.cn/problem/P5836



<div style='page-break-after:always;'></div> 

## 题解

这一棵树只有两种颜色，故我们只要记录树上一个个颜色相同的连通块，只有当所查询两点是同一连通块且连通块颜色与目标颜色不同时输出0。

使用并查集


![](assets/16165615776484.jpg)



```C++
#include <bits/stdc++.h>
using namespace std;
int N,  // 农场 <=10^5
	M;  //  朋友 <=10^5

int  f[100005];
struct Node{
	char cow;
	vector <int> child;
}farms[100005];
int ans[100005], cnt=0;

void makeset(int n)
{
	for(int i=1;i<=n;++i) f[i]=i;
}

int find(int x)
{
	if (f[x]==x) return x;
	return f[x]=find(f[x]);
}

void unionset(int x, int y)
{
	f[find(y)]=find(x);
}

int main()
{
	freopen("milkvisits2.in","r",stdin);
	int x,y;
	char c;
	cin>>N>>M;
	makeset(N);
	for(int i=1;i<=N;++i) cin>>farms[i].cow;
	for(int i=1;i<N;++i)
	{
		cin>>x>>y;
		farms[x].child.push_back(y);
		farms[y].child.push_back(x);
		if(farms[x].cow == farms[y].cow) unionset(x,y);
	}
	for(int i=1;i<=M;++i)
	{
		cin>>x>>y>>c;
		if(farms[find(x)].cow==c || farms[find(y)].cow==c || find(x)!=find(y))
			ans[++cnt]=1;
		else
			ans[++cnt]=0;
	}
	for(int i=1;i<=cnt;++i) cout<<ans[i];
	cout<<endl;
	return 0;
}
```

后记

其实不用建树，直接并查集就可以。