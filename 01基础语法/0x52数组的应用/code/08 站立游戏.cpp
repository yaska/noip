#include <bits/stdc++.h>
using namespace std;
const int MAXN=10001;
bool a[MAXN];  //默认值为 false,表示站立；取反为true，表示坐下。 
int main()
{
	int n, k, i, j;
	cin >> n >> k;
	for(i=2; i<=k; i++)   //执行k-1次操作
		for(j=1; j<=n/i; j++)
			a[i*j]=!a[i*j];
			
	for(i=1; i<=n; i++)
		if (!a[i])
			cout << i << " ";
	cout << endl;
	return 0;
}
			 
