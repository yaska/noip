#include <bits/stdc++.h>
using namespace std;
#define N 20
int a[N+5];
int n;

int main()
{
	int t;
	bool flag; //标记本轮是否发生过交换 
	cin>>n;
	for(int i=1;i<=n;++i) cin>>a[i];
	
	for(int i=1;i<n;++i)
	{
		flag=false; 
		for(int j=1;j<=n-i;++j)
		{
			if(a[j]<a[j+1])
			{
				t=a[j];
				a[j]=a[j+1];
				a[j+1]=t;
				flag=true;
			}
		 }
		 if(!flag) break; 
	}
	for(int i=1;i<=n;++i) cout<<a[i]<<"\n";
	return 0; 
}
