#include <bits/stdc++.h>
using namespace std;
int a[100005],n,ans;
int b[100005];  // b[i]为数列a的前缀和，b[i]=a[1]+a[2]+...+a[i]
int main()
{
	int tmp;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		b[i]=b[i-1]+a[i];
	}
	ans=a[1];
	for(int i=1;i<=n;i++)     //i为起点 
		for(int j=i;j<=n;j++) //j为终点
		{
			tmp=b[j]-b[i-1]; //求子段和
			ans=max(ans,tmp); 
		}
	cout<<ans<<endl;
	return 0;
} 
	
//  15/20
 
