# 函数

## 绝对素数

如果一个自然数是素数,且它的数字位置经过对换后仍为素数,则称为绝对素数,例如13。试求出所有二位绝对素数。

### 题目分析

素数，也称质数，是只能被1和自身整除的自然数。

对于这样的题目，数值不大，我们可以直接用模拟的方法，穷举10~99之间的所有整数x，求出x的个位和十位交换的后的整数y，再判断x和y是不是同时为素数。

写出伪代码框架如下：

```C++
int main()
{
	int x, y;
	for(int i=10;i<=99;++i)
	{
		x=i;
		y=x的个位十位交换;
		if(x是素数 && y是素数)
		    cout<<i<<" ";
	}
	return 0;
}
```

再“逐步求精”，细化完成细节：

```C++
#include <bits/stdc++.h>
using namespace std;
int main()
{
	int x, y;
	for(int i=10;i<=99;++i)
	{
		x=i;
		//y=x的个位十位交换;
		int ge=x%10;
		int shi=x/10;
		y=ge*10+shi;
		
		//判断x和y是否是素数
		bool isprimex=true;
		for(int k=2;k*k<=x;++k)
		{
			if(x%k==0)
			{
				isprimex=false;
				break;
			}
		}
		
		bool isprimey=true;
		for(int k=2;k*k<=y;++k)
		{
			if(y%k==0)
			{
				isprimey=false;
				break;
			}
		}
		
		//if(x是素数 && y是素数)
		if(isprimex && isprimey)
		    cout<<i<<" ";
	}
	return 0;
}


```

虽然完成了题目，但是有几点不好：

1. 主程序main()变得相当长，阅读理解起来很困难；
2. 为了解决问题，引入了很多变量，这些变量只是在解决局部问题用一下，和整体问题无关；
3. 判断x和y是不是素数，重复使用了大量代码。

为此，我们引入函数功能。函数是带有名字的一小段代码，实现特定功能，使用时用函数名字代替整段函数代码。函数定义如下：

```C++
返回类型  函数名（形式参数列表）
{
	   函数体；
    return 函数值;
}
```


先写函数实现“求x的个位十位交换的数”。提供的数据是x，需要得到y，都是整数。得到函数定义：`int jiaohuan(int a)` ，a是形式参数，在函数使用时，会用具体的值代替。

![](media/16100837722543.jpg)



最后完成函数主体

```c++
int jiaohuan(int a) 
{
    int ge,shi,b;
    ge=a%10;
    shi=a/10;
    b=ge*10+shi;
    return b;
}
```

在主程序中调用这个函数
```C++
int jiaohuan(int a){......} //定义内容放在调用前
int main()
{
	int x, y;
	for(int i=10;i<=99;++i)
	{
		x=i;
		//y=x的个位十位交换;
		y=jiaohuan(x);    //调用函数
		if(x是素数 && y是素数)
		    cout<<i<<" ";
	}
	return 0;
}
```

![](media/16100845842089.jpg)

同样，可以定义一个函数来判断变量a是不是素数。

![](media/16100908942443.jpg)



```C++
______ isprime(________)  //a是素数返回真，否则返回假
{
    for(int i=2;i*i<=a;++i)
        if(a%i==0)
            return false;
    return true;
}

......
int main()
{
    ......
    //if(x是素数 && y是素数)
    if(____________&&__________)
    ......
}
```


完整代码如下：
```C++
#include <iostream>
using namespace std;

int jiaohuan(int a)
{
    int ge, shi, b;
    ge=a%10;
    shi=a/10;
    b=ge*10+shi;
    return b;
}

bool isprime(int a)  //a是素数返回真，否则返回假
{
    for(int i=2;i*i<=a;++i)
        if(a%i==0)
            return false;
    return true;
}

int main()
{
    int x,y;
    for(int i=10;i<=99;++i)
    {
        x=i;
        y=jiaohuan(x);
        if( isprime(x) && isprime(y))
            cout<<i<<" ";
    }
    return 0;
}
```

<div style='page-break-after:always;'></div> 

## 素数对

两个相差为2的素数称为素数对，如5和7，17和19等，本题目要求找出所有两个数均不大于n的素数对。

### 输入

一个正整数n(1≤n≤10000)。

### 输出

所有小于等于n的素数对。每对素数对输出一行，中间用单个空格隔开。若没有找到任何素数对，输出empty。

### 样例输入

```
100
```



### 样例输出

```
3 5
5 7
11 13
17 19
29 31
41 43
59 61
71 73
```



<div style='page-break-after:always;'></div> 





