#include <bits/stdc++.h>
using namespace std;
#define N 20
int a[N+5][N+5];
int n;
int main()
{
	cin>>n;
	for(int i=1; i<=n; ++i)			//从下标[1][1]作为左上角起点 
		for(int j=1; j<=n; ++j)
		{
			cin>>a[i][j];
			if (i==j || i+j==n+1 ) a[i][j]+=10;  //对角线 
		}
	for(int i=1; i<=n; ++i)
	{
		for(int j=1; j<=n; ++j)
			cout<<a[i][j]<<" ";
		cout<<"\n";  //换行
	}
	return 0;
} 
