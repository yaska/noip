#include <bits/stdc++.h>
using namespace std;
#define N 20
int a[N+5][N+5];
int n;
int main()
{
	cin>>n;
	a[1][1]=1;
	for(int i=2; i<=n; ++i)			//从第2行开始填充
	{
		a[i][1]=1;		//每行第1个数是1 
		a[i][i]=1; 		//    最后一个数也是1		
		for(int j=2; j<=i-1; ++j)  //填中间位置，第i行最多有i个数 
			a[i][j]=a[i-1][j-1]+a[i-1][j]; 
	}
	for(int i=1; i<=n; ++i)
	{
		for(int j=1; j<=i; ++j)
			cout<<a[i][j]<<" ";
		cout<<"\n";  //换行
	}
	return 0;
} 
