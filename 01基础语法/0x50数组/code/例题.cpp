#include <bits/stdc++.h>
#define N 50 
using namespace std;
int a[N+5];		//多定义5个，即a[0]到a[54] 
double ave;		//平均成绩 
int sum=0;		//总分 
int main()
{
	for(int i=1;i<=N;++i) 
	{
		cin>>a[i]; //成绩记录在a[1]到a[50] 
		sum += a[i];
	}
	ave = (double)sum/N;
	for(int i=1;i<=N;++i)
	{
		if (a[i]<ave)
			cout<<i<<" "<<a[i]<<endl;
	}
	return 0;
}
