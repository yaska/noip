#include <bits/stdc++.h>
#define N 10 
using namespace std;
int a[N];	//定义了a[0]..a[9]，一共10个整型变量 
int main()
{
	for(int i=0;i<N;++i)	//数组是多个变量，因此需要循环来访问整个数组 
		a[i] = 2+i*2;		//利用循环变量i来顺序访问数组元素 
	a[7] = 7;				//访问单个数组元素 
	for(int i=0;i<N;++i)
		cout<<a[i]<<" ";
	return 0;
 } 
