#include <bits/stdc++.h>
using namespace std;
int days[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};//初始化
int ans=0;
int main()
{
	int year,month,day;
	cin>>year>>month>>day;
	for(int i=1;i<month;i++)  //加上month之前的月份天数
		ans+=days[i];
	ans+=day;             //加上天数
	if((year%4==0) && year%100!=0 || (year%400==0))
		if(month>2) ans++;   //闰年2月后，多加1天
	cout<<ans<<endl;
	return 0;
}

